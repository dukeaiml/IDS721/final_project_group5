// Import necessary libraries
use actix_web::{web, App, HttpResponse, HttpServer, Responder, post};
use rust_bert::pipelines::summarization::SummarizationModel;
use serde::{Deserialize, Serialize};
use tracing::info;
use tracing_subscriber::FmtSubscriber;
use actix_web_prom::PrometheusMetrics;
use std::collections::HashMap;


// Define the structure of the input data to be received
#[derive(Serialize, Deserialize)]
struct Input {
    text: String,
}

// Define the structure of the output data to be returned
#[derive(Serialize, Deserialize)]
struct Output {
    summary: Vec<String>,
}

// Define the endpoint for summarization
#[post("/summarize")]
async fn summarize(query: web::Json<Input>) -> impl Responder {
    // Clone the input text from the query
    let input = query.text.clone();
    // Create a new summarization model and use it to summarize the input text
    // This is done in a separate thread using web::block to prevent blocking the server
    let summarize_result = web::block(move || {
        let summarization_model = SummarizationModel::new(Default::default()).expect("Error creating model");
        let input = [input.as_str()];
        // summarization_model.summarize(&input)
        // Ok(summarization_model.summarize(&input))
        Ok::<_, std::io::Error>(summarization_model.summarize(&input))

    })
    .await;

    // Check the result of the summarization
    match summarize_result {
        // If the summarization was successful, format the output and return it
        Ok(output) => {
            let mut response = String::new();
            for summary in output {
                response.push_str(&format!("Summary: {:?}\n", summary));
            }
            HttpResponse::Ok().content_type("text/plain").body(response)
        },
        Err(_) => {
            HttpResponse::InternalServerError().content_type("text/plain").body(format!("Operation Error!"))
        }
    }
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // let prometheus = PrometheusMetrics::new("api", "/metrics");
    let prometheus = PrometheusMetrics::new("api", Some("/metrics"), Some(HashMap::new()));    
    // Set up the subscriber
    let subscriber = FmtSubscriber::builder()
        .with_max_level(tracing::Level::INFO)
        .finish();

    // Set the global default subscriber
    tracing::subscriber::set_global_default(subscriber)
        .expect("setting default subscriber failed");

    // Log an info message
    info!("Starting server at 0.0.0.0:8080");

    HttpServer::new(move || {
        App::new()
            .wrap(prometheus.clone())
            .service(summarize)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use actix_web::{test, App};
    use super::*; // Import the necessary components from the parent module

    #[actix_rt::test]
    async fn test_summarize_endpoint() {
        let sample_text = "North Carolina is a southeastern U.S. state with a diverse landscape ranging from Atlantic Ocean beaches to Appalachian Mountains. It's known for its rich history, thriving tech industry, and significant contributions to American music and sports.";
        let input = Input { text: sample_text.to_string() };

        let mut app = test::init_service(
            App::new().service(summarize)
        ).await;

        let req = test::TestRequest::post()
            .uri("/summarize")
            .set_json(&input)
            .to_request();

        let resp = test::call_service(&mut app, req).await;
        assert!(resp.status().is_success(), "Expected success response");

        let body = test::read_body(resp).await;
        let body_str = std::str::from_utf8(&body).expect("Response should be valid UTF-8");

        println!("Test Input: {}", sample_text);
        println!("Test Output: {}", body_str);
        // Check if the response contains an expected keyword or phrase from the summary
        assert!(body_str.contains("North Carolina"), "Summary should mention 'North Carolina'");
    }

    #[actix_rt::test]
    async fn test_summarize_endpoint_docker() {
        let sample_text = "Duke University, located in Durham, North Carolina, is a prestigious private research institution known for its strong academic programs, especially in law, business, and medicine. It boasts a beautiful Gothic architecture campus and a renowned basketball team.";
        let input = Input { text: sample_text.to_string() };

        let mut app = test::init_service(
            App::new().service(summarize)
        ).await;

        let req = test::TestRequest::post()
            .uri("/summarize")
            .set_json(&input)
            .to_request();

        let resp = test::call_service(&mut app, req).await;
        assert!(resp.status().is_success(), "Expected success response");

        let body = test::read_body(resp).await;
        let body_str = std::str::from_utf8(&body).expect("Response should be valid UTF-8");

        println!("Test Input: {}", sample_text);
        println!("Test Output: {}", body_str);
        assert!(body_str.contains("Duke University"), "Summary should mention 'Duke University'");
    }

}