# Final Project - Group 5

## Project Description
The project is to build a Rust API that uses the RustBert library to summarize text. The API will take a text input and return a summarized version of the text. The API will be containerized using Docker and deployed on Kubernetes. The CI/CD pipeline will be implemented using GitLab service. The application will be monitored using Prometheus and Grafana. The project will be documented using Markdown and the README file will contain a link to a video demonstrating the application.

## Team Members
- Zhihan Xu 
- Yanbo Guan
- Zichun Wang
- Zihan "Zach" Xing

## Project Demo Video
Video deomo at YouTube link [https://youtu.be/PuCmkZRQYYU](https://youtu.be/PuCmkZRQYYU).
## Features
- Text Summarization: Provides an endpoint to submit text and receive a condensed version, simplifying content comprehension.
- Concurrency Handling: Utilizes web::block to offload blocking operations, ensuring the server remains responsive.
- Error Handling: Robust error handling that provides clear failure messages in cases where the summSarization cannot be performed.
- Prometheus Metrics: Integrated Prometheus metrics for monitoring the API's usage and performance.
- Testing: Includes integration tests to ensure endpoint reliability and correctness.

## Checklist
- Obtain Open Source ML Model: We used the RustBert library to summarize text.
- Create Rust Web Service for Model Inferences: We created a Rust API that uses the RustBert library to summarize text.
- Containerize Service and Deploy to Kubernetes: We containerized the Rust API using Docker and deployed it on Kubernetes.
- Implement CI/CD Pipeline: We implemented a CI/CD pipeline using Gitlab service.

## Prerequisites
1. Install libtorch via [https://download.pytorch.org/libtorch/cu118/libtorch-cxx11-abi-shared-with-deps-2.0.0%2Bcu118.zip](https://download.pytorch.org/libtorch/cu118/libtorch-cxx11-abi-shared-with-deps-2.0.0%2Bcu118.zip)
2. Install openssl via ```sudo apt install openssl```

## Dependencies
```
[dependencies]
actix-web = "3.0.0"
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
actix-rt = "2.5.0"
rust-bert = "0.21.0"
prometheus = { version = "0.13", default-features = false }
tracing = "0.1"
tracing-subscriber = "0.2"
actix-web-prom = "0.5.0"

```
## Run Local Test:
To run the RustBert API locally, you can follow the steps below:
1. Clone the repository
```bash
git clone git@gitlab.com:dukeaiml/IDS721/final_project_group5.git
```
2. Change the directory
```bash
cd final_project_group5
```
3. Run the RustBert API
```bash
cargo run
```
4. Test the API using curl
```bash
curl -X POST -H "Content-Type: application/json" -d '{"text": "Roughly 20 environmental protesters were arrested at a Massachusetts airport on Saturday morning. Protesters supporting Extinction Rebellion crossed into a security area and were arrested. Some of the protesters breached a security perimeter on a tarmac at Hanscom Field Airport in Bedford, Massachusetts. The arrested protesters could face trespassing and disorderly conduct charges. The Massachusetts Port Authority said the airport had to temporarily shut down for security reasons. Extinction Rebellion were protesting the expansion of 17 new private jet hangers at the airport. Protesters stood in front of some airplanes to block movement. The Hanscom Airport expansion project is currently under review, said the Governor of Massachusetts, Maura Healey to NewsCenter 5. The Bedford Police Department said no one is in danger. CNN tried to contact the Massachusetts Port Authority but had received no response."}' http://127.0.0.1:8080/summarize
```
5. Run local test
``` cargo test -- --nocapture ```

### Results of Local Test:
- We provide two local test that send two sample paragraphs to our summarizer, the original and summarized output is also shown, it can be observed that the results meet the expectation and the tests are also passed!
![alt text](/images/local_test.png)

## Deploy
For the deployment, we used Docker and Kubernetes(K8s). Docker is for containerization and Kubernetes is for orchestration.

The steps to deploy the RustBert API using Docker and Kubernetes are as follows:

### Docker
1. Build the Docker image
```bash
docker build -t rustbert .
```
2. Tag the Docker image
```bash
docker tag rustbert:latest 0xhzx/rustbert:latest
```
3. Run the Docker image locally
```bash
docker run -p 8080:8080 rustbert:latest
```
4. Push the Docker image to Docker Hub
```bash
docker push 0xhzx/rustbert:latest
```

### K8s
- Prepare Your AWS Environment (Install AWS CLI, kubeclt, eksctl, etc.) [Ref1](https://eksctl.io/installation/#direct-download-latest-release-amd64x86_64-armv6-armv7-arm64), [Ref2](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

- Create an EKS Cluster
`eksctl create cluster --name ids721-cluster --region us-east-1`
![alt text](/images/image-5.png)

- Configure kubectl to Connect to Your EKS Cluster
`aws eks --region us-east-1 update-kubeconfig --name ids721-cluster`
- Apply k8s deployment and service
```bash
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```
![alt text](/images/image-6.png)

![alt text](/images/image-4.png)
![alt text](/images/image-7.png)

## API Test
Here are the screenshots of the API test using Postman and the K8s endpoint
![alt text](/images/image-8.png)

![alt text](/images/image-9.png)

## Main functionalities screenshots
- Use trace and tracing-subscriber for log tracing
![alt text](/images/image-1.png)
- Post request for summarize endpoint
![alt text](/images/image.png)
- To monitor the application, we use `actix_web_prom::PrometheusMetrics`, which is a middleware that provides Prometheus metrics for Actix Web applications. Following is the output of Prometheus metrics, which provides information about the HTTP requests of your application. This information includes the number of requests and the duration of requests.

1.  api_http_requests_duration_seconds_bucket: This is a histogram that shows the distribution of request durations. For example, api_http_requests_duration_seconds_bucket{endpoint="/metrics",method="GET",status="200",le="0.005"} 1 means there was 1 GET request to the /metrics endpoint with a status code of 200, and the duration was less than or equal to 0.005 seconds.

2. api_http_requests_duration_seconds_sum: This is the total sum of request durations. For example, api_http_requests_duration_seconds_sum{endpoint="/metrics",method="GET",status="200"} 0.0000968 means the total duration of GET requests to the /metrics endpoint with a status code of 200 was 0.0000968 seconds.

3. api_http_requests_duration_seconds_count: This is the total number of requests. For example, api_http_requests_duration_seconds_count{endpoint="/metrics",method="GET",status="200"} 1 means there was 1 GET request to the /metrics endpoint with a status code of 200.

4. api_http_requests_total: This is the total number of requests for each endpoint, method, and status. For example, api_http_requests_total{endpoint="/metrics",method="GET",status="200"} 1 means there was 1 GET request to the /metrics endpoint with a status code of 200.

![alt text](/images/image-2.png)

- Docker run
![alt text](/images/image-3.png)

- CICD pipeline
![alt text](/images/image-10.png)

## Group Work Clarification
For this project, we were able to work together as a team to complete the project. Most of the work was done in several sessions where we met on Zoom to discuss the project and work on the code together. To avoid trivial issues on environment setup, we ran all the code on the same machine, so that our commits might not be evenly distributed.